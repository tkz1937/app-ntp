Project name clearos app-ntp
-----------------------------------------------------------------------------------------------------------
the programming language used in this project is PHP

NTP servers are used to synchronize time on systems and ensure that they run their scheduling tools 
and applications without conflict,by draining system resources and even overloading hardware. 
It is also worth mentioning that there is sometimes a chance that the connection to the country, state,
city or even location and the ISP does not reach the ClearOS / ClearCenter NTP servers,but you need everything from even keep the timing.
and this app allows you to delete, edit and add a server


License
-----------------------------------------------------------------------------------------------------------

this project project use LGPLv3 licence


Using it 
--------------------------------------------------------------------------------------------------------------

the apps will be use in clearos

how to use it just first have clearos install and configure it more up to date and then download it from the clearos market place or go to the directory at the following address: / usr / clearos / apps
then clone the project in the apps directory
and to make it easier to use and avoid file conflicts when using ntp server go
in the directory / etc / and open the file ntp.conf then open it, then take all the address serve there put at the end of the file rest assured that they occupy the end of your file reassure you that you are in root mode (sudo) and give him all the rights to the file (rwx)
and then launch the application

Install it
-----------------------------------------------------------------------------------------------------------------
you will first have a machine with a good hard disk, a ram etc ...
and see the system centos version 7

the application will be installed in clearos

secondly you can install on the system and after referring to how to use it

Contributing
-------------------------------------------------------------------------------------------------------------------

if you have any problems report to the following address:
 https://gitlab.com/tkz1937/ https://github.com/tkz1937/ https://github.com/orgs/itotafrica/tkz1937  e-mail: tekazayaeliel@gmail.com

to join our community go to:
 https://itotafrica.com/forum/ or https://www.clearos.com/clearfoundation/social/community



 Translators
 ---------------------------------------------------------------------------------------------------------------------

if you translate this project please go to the language folder of this project and create it in a folder that will support your translation module that you should write

Writers
-------------------------------------------------------------------------------------------------------------------------

if you add information after you have added or modify the project please go to the folder deploy of the project and open the file info and add this the what goes with what you did